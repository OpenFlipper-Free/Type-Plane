/*===========================================================================*\
*                                                                            *
*                              OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
*                                                                            *
\*===========================================================================*/

#include "QtPlaneSelect.hh"

#include <ACG/Scenegraph/GlutPrimitiveNode.hh>
#include <OpenFlipper/BasePlugin/BaseInterface.hh>
#define PLUGINFUNCTIONS_C
#include <OpenFlipper/BasePlugin/PluginFunctions.hh>

#include <ACG/QtWidgets/QtColorTranslator.hh>

/*******************************************************************************
         Initialization and de initialization
*******************************************************************************/
QtPlaneSelect::QtPlaneSelect( ACG::GLState& glState )
  : glState( glState ),
    nodeIdx_(0),
    targetIdx_(0),
    isDragging_( false ),
    planeNode_(0)
{

}

QtPlaneSelect::~QtPlaneSelect( )
{
}


/*******************************************************************************
        Implementation of public slots
*******************************************************************************/

void QtPlaneSelect::slotKeyReleaseEvent(QKeyEvent* event)
{
  if (event->key() == Qt::Key_Escape){
    if( planeNode_ )
    {
        planeNode_->delete_subtree( );
        planeNode_ = 0;
    }

    emit updateViewProxy( );

    // Trigger the event
    isDragging_ = false;
  }
}

void QtPlaneSelect::slotMouseEvent(QMouseEvent* event)
{
//  unsigned int width  = glState.viewport_width();
  unsigned int height = glState.viewport_height();

  //cancel on rightclick
  if (event->button() == Qt::RightButton){
    if( planeNode_ )
    {
        planeNode_->delete_subtree( );
        planeNode_ = 0;
    }

    emit updateViewProxy( );

    // Trigger the event
    isDragging_ = false;

    return;
  }


  switch( event->type() )
  {
    case QEvent::MouseButtonPress:
    {

      // Only react on the left button and ignore the others
      if ( event->button() != Qt::LeftButton )
        return;

      
      size_t     node_idx, target_idx;

      if (PluginFunctions::scenegraphPick(ACG::SceneGraph::PICK_FACE,
                                            event->pos(),
                                            node_idx,
                                            target_idx,
                                            &sourcePoint3D))
      {

        // Shift toward viewer
        //sourcePoint3D = sourcePoint3D + PluginFunctions::viewingDirection();

        isDragging_ = true;

        if ( planeNode_ == 0 ) {
          planeNode_ = new PlaneNode(plane_,PluginFunctions::getRootNode(),"PolyLine generation Plane"  );
        }

            /// remember the 2d click position (for clamping)
            sourcePoint2D = ACG::Vec3d(event->pos().x(), height - event->pos().y() - 1.0, 0.0);

            setPlaneAndSize(sourcePoint3D, sourcePoint2D);

        planeNode_->show();
        emit nodeVisChangedProxy(planeNode_->id());

        nodeIdx_   = node_idx;
        targetIdx_ = target_idx;


        emit updateViewProxy( );
      }
    }break;
    case QEvent::MouseMove:
      {
          if( isDragging_ )
          {
            setPlaneAndSize(sourcePoint3D,ACG::Vec3d(event->pos().x(), height-event->pos().y()-1.0, 0.0));

              emit updateViewProxy( );
          }
      } break;

  case QEvent::MouseButtonRelease:
      {
          if( isDragging_ )
          {
              if( planeNode_ )
              {
                  planeNode_->delete_subtree( );
                  planeNode_ = NULL;
              }

              emit updateViewProxy( );

              emit( signalTriggerCut( ) );
              // Trigger the event
              isDragging_ = false;
          }
      } break;

  default:
      break;
  }


}


void QtPlaneSelect::setPlaneAndSize(const ACG::Vec3d& _sourcePoint3D,const ACG::Vec3d& _target2D)
{

  ACG::Vec3d source2D = glState.project( _sourcePoint3D );
    source2D[2] = 0.;

    const ACG::Vec3d diff = source2D - _target2D;

    ACG::Vec3d ortho{0., 0., 0.};
    if (clamp_)
    {
        constexpr auto clamp_angle = M_PI / 12.;                   /// angle (15°) at which we clamp in radians
        const auto newTarget = _target2D - source2D;               /// center towards origin
        const auto angle = std::atan2(newTarget[1], newTarget[0]); /// compute angle in CCW dir to the x-axis
        const auto remainder = std::fmod(angle, clamp_angle);      /// difference to next multiple of clamped angle

        /// compute corrected angle
        double corrected_angle = .0;
        if (remainder < clamp_angle / 2.)
            corrected_angle = angle - remainder; /// if we are at the lower half, clamp down
        else
            corrected_angle = angle + (clamp_angle - remainder); /// if we are at the upper half, clamp up

        const auto radius = diff.length();
        const auto newTarget2D = ACG::Vec3d{std::round(radius * std::cos(corrected_angle)), std::round(radius * std::sin(corrected_angle)), 0.} + source2D;
        const auto newDiff = source2D - newTarget2D;
        ortho = ACG::Vec3d{-newDiff[1], newDiff[0], 0.};
    }
    else
  //diff.normalize( );  <- this is bad
        ortho = ACG::Vec3d{-diff[1], diff[0], 0.};

    /// construct two vectors in the plane, orthogonally to each other
    const auto left = glState.unproject(source2D + ortho * 10. + ACG::Vec3d{0., 0., 0.});
    const auto right = glState.unproject(source2D - ortho * 10. + ACG::Vec3d{0., 0., 0.});

    const auto leftvec = (left - sourcePoint3D).normalized();
    const auto rightvec = (right - sourcePoint3D).normalized();

    /// compute normal vector
    normal_ = cross(rightvec, leftvec);
    normal_.normalize();

    //  std::cout << "computed normal: " << normal_ << std::endl;

    /// create plane using point and normal
    const auto sourcePoint3Df(sourcePoint3D);
    const auto normald(normal_);
  plane_.setPlane(sourcePoint3Df,normald);
  plane_.setSize(PluginFunctions::sceneRadius(),PluginFunctions::sceneRadius());
  planeNode_->update();
}




